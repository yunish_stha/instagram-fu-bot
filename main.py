from getpass import getpass
from time import sleep

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager


class InstagramBot:
    def __init__(self, username, pw):
        self.driver = webdriver.Chrome(ChromeDriverManager().install())
        self.username = username
        self.driver.get("https://instagram.com")
        sleep(2)
        try:
            input_login = self.driver.find_element_by_xpath("//input[@name=\"username\"]").send_keys(username)
            input_login = self.driver.find_element_by_xpath("//input[@name=\"password\"]").send_keys(pw)
            submit = self.driver.find_element_by_xpath("//button[@type=\"submit\"]").click()
            sleep(4)
            self.driver.find_element_by_xpath("//button[contains(text(), 'Not Now')]").click()
            sleep(2)
            self.driver.find_element_by_xpath("//button[contains(text(), 'Not Now')]").click()
            sleep(2)
        except Exception as e:
            print("Username or password Incorrect...", e)

    def get_unfollowers(self):
        self.driver.find_element_by_xpath("//a[contains(@href,'/{}')]".format(self.username)).click()
        sleep(2)
        self.driver.find_element_by_xpath("//a[contains(@href, '/{}/following/')]".format(self.username)).click()
        # suggest = self.driver.find_element_by_xpath('//h4[contains(text(), Suggestions)]')
        # self.driver.execute_script('arguments[0].scrollIntoView()', suggest)
        following = self._get_names()
        self.driver.find_element_by_xpath("//a[contains(@href, '/{}/followers/')]".format(self.username)).click()
        followers = self._get_names()
        not_following_back = [user for user in following if user not in followers]
        print('Not Followed By: ', not_following_back)
        print('Total Count: ', len(not_following_back))

    def _get_names(self):
        sleep(2)
        scroll_box = self.driver.find_element_by_xpath("/html/body/div[4]/div/div/div[2]")
        last_height, height = 0, 1
        while last_height != height:
            last_height = height
            sleep(1)
            height = self.driver.execute_script("""
                   arguments[0].scrollTo(0, arguments[0].scrollHeight);
                   return arguments[0].scrollHeight;""", scroll_box)
        links = scroll_box.find_elements_by_tag_name('a')
        names = [name.text for name in links if name.text != '']
        self.driver.find_element_by_xpath('/html/body/div[4]/div/div/div[1]/div/div[2]/button').click()
        return names


if __name__ == '__main__':
    username = input('Enter your instagram username: ')
    password = getpass('Enter your instagram password: ')

    bot = InstagramBot(username, password)
    try:
        bot.get_unfollowers()
    except:
        print('Error Login!..')
